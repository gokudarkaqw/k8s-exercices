variable "token" {}
variable "ssh_key" {}

variable "image" {
  default = "ubuntu-18-04-x64"
}
variable "size" {
  default = "8gb"
}
variable "region" {
  default = "lon1"
}
variable "node_number" {
  default = 3
}
